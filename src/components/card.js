import { h, Component } from 'preact';

export default function Card(props){
        console.log(this.props);
        return <div class="demo-card-square mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title mdl-card--expand" >
              
            </div>
            <div class="mdl-card__supporting-text">
              <img src={this.props.repository.owner.avatar_url} style="height:100px;width:100px;border-radius:50%;"/>
              {this.props.repository.description}
            </div>
            <div class="mdl-card__actions mdl-card--border">
              <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                {this.props.repository.stargazers_count} Stars
              </a>
            </div>
          </div>;
    
}
