import { h, Component } from 'preact';
import style from './style';
import Card from '../../components/card';
export default class Profile extends Component {
	 componentDidMount(){
		let result =  fetch('https://api.github.com/users/facebook/repos');
		let json =  result.json();
		this.setState({
			repos: json,
			isloading: false
		})
	}
	constructor(){
		super();
		this.setState({
			isloading: true
		})
	}
	render() {
		let load = this.state.isloading;
		let repository = this.state.repos;
		//console.log(load + " repos is "+repository);
		return ( 	<div>
			{ !load && 
				repository.map(repo => (
				<div class={style.home}>
					<Card repository={repo}/>
				</div>
				)
		)}
		</div>);
	}
}
